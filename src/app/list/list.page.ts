import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {
  private selectedItem: any;
  private icons = [
    'flask',
    'wifi',
    'beer',
    'football',
    'basketball',
    'paper-plane',
    'american-football',
    'boat',
    'bluetooth',
    'build'
  ];
  public items: Array<any> = [];
  constructor() {
    this.items = 
    [
      { "Infinitive" : "Be" , "SimplePast" : "Was/were", "PastParticiple" : "Been", "Significado" : "Ser" },
      { "Infinitive" : "Bear" , "SimplePast" : "Bore", "PastParticiple" : "Born/Borne", "Significado" : "Suportar" },
      { "Infinitive" : "Beat" , "SimplePast" : "Beat", "PastParticiple" : "Beaten", "Significado" : "Bater/vencer" },
      { "Infinitive" : "Become" , "SimplePast" : "Became", "PastParticiple" : "Become", "Significado" : "Tornar-se" },
      { "Infinitive" : "Begin" , "SimplePast" : "Began", "PastParticiple" : "Begun", "Significado" : "Começar" },
      { "Infinitive" : "Bite" , "SimplePast" : "Bit", "PastParticiple" : "Bitten", "Significado" : "Morder" },
      { "Infinitive" : "Bleed" , "SimplePast" : "Bled", "PastParticiple" : "Bled", "Significado" : "Sangrar" },
      { "Infinitive" : "Break" , "SimplePast" : "Broke", "PastParticiple" : "Broken", "Significado" : "Quebrar" },
      { "Infinitive" : "Build" , "SimplePast" : "Built", "PastParticiple" : "Built", "Significado" : "Construir" },
      { "Infinitive" : "Catch" , "SimplePast" : "Caught", "PastParticiple" : "Caught", "Significado" : "Pegar/capturar" },
      { "Infinitive" : "Choose" , "SimplePast" : "Chose", "PastParticiple" : "Chosen", "Significado" : "Escolher" },
      { "Infinitive" : "Cling" , "SimplePast" : "Clung", "PastParticiple" : "Clung", "Significado" : "Unir-se" },
      { "Infinitive" : "Deal" , "SimplePast" : "Dealt", "PastParticiple" : "Dealt", "Significado" : "Negociar/lidar/dar as cartas" },
      { "Infinitive" : "Dig" , "SimplePast" : "Dug", "PastParticiple" : "Dug", "Significado" : "Cavar" },
      { "Infinitive" : "Do" , "SimplePast" : "Did", "PastParticiple" : "Done", "Significado" : "Fazer" },
      { "Infinitive" : "Draw" , "SimplePast" : "Drew", "PastParticiple" : "Drawn", "Significado" : "Desenhar/traçar" },
      { "Infinitive" : "Drink" , "SimplePast" : "Drank", "PastParticiple" : "Drunk", "Significado" : "Beber" },
      { "Infinitive" : "Drive" , "SimplePast" : "Drove", "PastParticiple" : "Driven", "Significado" : "Dirigir/guiar" },
      { "Infinitive" : "Eat" , "SimplePast" : "Ate", "PastParticiple" : "Eaten", "Significado" : "Comer" },
      { "Infinitive" : "Fall" , "SimplePast" : "Fell", "PastParticiple" : "Fallen", "Significado" : "Cair" },
      { "Infinitive" : "Feel" , "SimplePast" : "Felt", "PastParticiple" : "Felt", "Significado" : "Sentir" },
      { "Infinitive" : "Fight" , "SimplePast" : "Fought", "PastParticiple" : "Fought", "Significado" : "Lutar" },
      { "Infinitive" : "Find" , "SimplePast" : "Found", "PastParticiple" : "Found", "Significado" : "Encontrar/descobrir" },
      { "Infinitive" : "Fly" , "SimplePast" : "Flew", "PastParticiple" : "Flown", "Significado" : "Voar" },
      { "Infinitive" : "Forbid" , "SimplePast" : "Forbade", "PastParticiple" : "Forbidden", "Significado" : "Proibir/impedir" },
      { "Infinitive" : "Forget" , "SimplePast" : "Forgot", "PastParticiple" : "Forgotten", "Significado" : "Esquecer" },
      { "Infinitive" : "Forgive" , "SimplePast" : "Forgave", "PastParticiple" : "Forgiven", "Significado" : "Perdoar" },
      { "Infinitive" : "Give" , "SimplePast" : "Gave", "PastParticiple" : "Given", "Significado" : "Dar" },
      { "Infinitive" : "Have" , "SimplePast" : "Had", "PastParticiple" : "Had", "Significado" : "Ter/possuir" },
      { "Infinitive" : "Hide" , "SimplePast" : "Hid", "PastParticiple" : "Hidden", "Significado" : "Ocultar/esconder" },
      { "Infinitive" : "Hold" , "SimplePast" : "Held", "PastParticiple" : "Held", "Significado" : "Segurar/aguardar" },
      { "Infinitive" : "Kneel" , "SimplePast" : "Knelt", "PastParticiple" : "Knelt", "Significado" : "Ajoelhar-se" },
      { "Infinitive" : "Know" , "SimplePast" : "Knew", "PastParticiple" : "Known", "Significado" : "Saber" },
      { "Infinitive" : "Lay" , "SimplePast" : "Laid", "PastParticiple" : "Laid", "Significado" : "Colocar" },
      { "Infinitive" : "Leave" , "SimplePast" : "Left", "PastParticiple" : "Left", "Significado" : "Deixar/ir embora" },
      { "Infinitive" : "Lend" , "SimplePast" : "Lent", "PastParticiple" : "Lent", "Significado" : "Emprestar" },
      { "Infinitive" : "Lose" , "SimplePast" : "Lost", "PastParticiple" : "Lost", "Significado" : "Perder" },
      { "Infinitive" : "Make" , "SimplePast" : "Made", "PastParticiple" : "Made", "Significado" : "Fazer/criar" },
      { "Infinitive" : "Meet" , "SimplePast" : "Met", "PastParticiple" : "Met", "Significado" : "Conhecer" },
      { "Infinitive" : "Pay" , "SimplePast" : "Paid", "PastParticiple" : "Paid", "Significado" : "Pagar" },
      { "Infinitive" : "Ride" , "SimplePast" : "Rode", "PastParticiple" : "Ridden", "Significado" : "Andar de bicicleta/moto/a cavalo" },
      { "Infinitive" : "Ring" , "SimplePast" : "Rang", "PastParticiple" : "Rung", "Significado" : "Tocar sino ou telefone/ligar" },
      { "Infinitive" : "Run" , "SimplePast" : "Ran", "PastParticiple" : "Run", "Significado" : "Correr" },
      { "Infinitive" : "Say" , "SimplePast" : "Said", "PastParticiple" : "Said", "Significado" : "Dizer" },
      { "Infinitive" : "See" , "SimplePast" : "Saw", "PastParticiple" : "Seen", "Significado" : "Ver/olhar" },
      { "Infinitive" : "Seek" , "SimplePast" : "Sought", "PastParticiple" : "Sought", "Significado" : "Procurar/buscar" },
      { "Infinitive" : "Send" , "SimplePast" : "Sent", "PastParticiple" : "Sent", "Significado" : "Enviar" },
      { "Infinitive" : "Shake" , "SimplePast" : "Shook", "PastParticiple" : "Shaken", "Significado" : "Sacudir" },
      { "Infinitive" : "Show" , "SimplePast" : "Shower", "PastParticiple" : "Shown", "Significado" : "Mostrar/aparecer" },
      { "Infinitive" : "Sit" , "SimplePast" : "Sat", "PastParticiple" : "Sat", "Significado" : "Sentar-se" },
      { "Infinitive" : "Sow" , "SimplePast" : "Sowed", "PastParticiple" : "Sown", "Significado" : "Semear/espalhar" },
      { "Infinitive" : "Spell" , "SimplePast" : "Spelt", "PastParticiple" : "Spelt", "Significado" : "Soletrar" },
      { "Infinitive" : "Stand" , "SimplePast" : "Stood", "PastParticiple" : "Stood", "Significado" : "Ficar de pé" },
      { "Infinitive" : "Steal" , "SimplePast" : "Stole", "PastParticiple" : "Stolen", "Significado" : "Roubar" },
      { "Infinitive" : "Swim" , "SimplePast" : "Swam", "PastParticiple" : "Swum", "Significado" : "Nadar" },
      { "Infinitive" : "Take" , "SimplePast" : "Took", "PastParticiple" : "Taken", "Significado" : "Pegar/tirar/levar" },
      { "Infinitive" : "Throw" , "SimplePast" : "Threw", "PastParticiple" : "Thrown", "Significado" : "Arremessar" },
      { "Infinitive" : "Understand" , "SimplePast" : "Understood", "PastParticiple" : "Understood", "Significado" : "Entender/compreender" },
      { "Infinitive" : "Wear" , "SimplePast" : "Wore", "PastParticiple" : "Worn", "Significado" : "Vestir/usar" },
      { "Infinitive" : "Write" , "SimplePast" : "Wrote", "PastParticiple" : "Written", "Significado" : "Escrever" }
    ]
  }

  ngOnInit() {
  }
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
}
